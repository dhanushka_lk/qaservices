﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("Message")]
    public class Message
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }

        public int SenderId { get; set; }

        public Nullable<int> ReceiverId { get; set; }

        public DateTime SendDate { get; set; }

        public string MessageBody { get; set; }

        public int ProjectId { get; set; }

        public virtual User Sender { get; set; }

        public virtual User Receiver { get; set; }

        public virtual Project Project { get; set; }
    }
}
