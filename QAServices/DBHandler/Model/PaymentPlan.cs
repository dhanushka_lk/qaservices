﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("PaymentPlan")]
    public class PaymentPlan
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PaymentPlanId { get; set; }

        public string PaymentPlanName { get; set; }

        public string Description { get; set; }

        public int Installments { get; set; }

    }
}
