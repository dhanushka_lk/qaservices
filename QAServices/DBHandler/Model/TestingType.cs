﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("TestingType")]
    public class TestingType
    {
        public TestingType()
        {
            this.Projects = new HashSet<Project>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int TestingTypeId { get; set; }

        [Required]
        public string TestingTypeName { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }
}
