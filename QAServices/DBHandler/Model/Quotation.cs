﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("Quotation")]
    public class Quotation
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int QuotationId { get; set; }

        public int Estimation { get; set; }

        public decimal Budget { get; set; }

        public string Notes { get; set; }

        public bool Approved { get; set; }

    }
}
