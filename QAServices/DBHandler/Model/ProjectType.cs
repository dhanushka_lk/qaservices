﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("ProjectType")]
    public class ProjectType
    {
        public ProjectType()
        {
            this.Projects = new HashSet<Project>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ProjectTypeId { get; set; }

        [Required]
        public string ProjectTypeName { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }
}
