﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("Project")]
    public class Project
    {
        public Project()
        {
            this.Messages = new HashSet<Message>();
            this.TestingTypes = new HashSet<TestingType>();
            this.Payments = new HashSet<Payment>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ProjectId { get; set; }

        public int ProjectTypeId { get; set; }

        public string ProjectName { get; set; }

        public string Description { get; set; }

        public string Features { get; set; }

        public string HardwareReq { get; set; }

        public string SoftwareReq { get; set; }

        public string Limitations { get; set; }

        public string DocumentPath { get; set; }

        public int StatusId { get; set; }

        public Nullable<int> PaymentPlanId { get; set; }

        public int UserId { get; set; }

        public Nullable<int> QuotationId { get; set; }

        public string ProjectUrl { get; set; }

        public virtual ProjectType ProjectType { get; set; }

        public virtual PaymentPlan PaymentPlan { get; set; }

        public virtual User User { get; set; }

        public virtual Quotation Quotation { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public virtual ICollection<TestingType> TestingTypes { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
    }
}
