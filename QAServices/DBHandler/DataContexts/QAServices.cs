﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DBHandler.Model;

namespace DBHandler.DataContexts
{
    public class QAServices:DbContext
    {
        public QAServices()
            : base("QAServConn")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UsersInRole> UsersInRoles { get; set; }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentPlan> PaymentPlans { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectType> ProjectTypes { get; set; }
        public DbSet<Quotation> Quotations { get; set; }
        public DbSet<TestingType> TestingTypes { get; set; }
    }
}
