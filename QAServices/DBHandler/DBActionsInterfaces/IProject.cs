﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.DBActionsInterfaces
{
    interface IProject
    {
        List<Project> GetProjectList();
        List<Project> GetProjectList(int id);
        List<ProjectType> GetProjectTypeList();
        bool SaveProject(Project project);
        Project FindProject(int id);
        bool AddQuotation(int projectId, Quotation Quotation);
        Quotation GetQuotationDetails(int projectId);
    }
}
