﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.DBActionsInterfaces
{
    interface IUser
    {
        void CreateUser(User user);
        User FindUser(int id);
        string GetUserRole(string userName);
        bool RegisterUser(string username, string password, string company, string address, string country, string contactno, string email, string userRole, string name);
        Tuple<bool, string> CheckUserLogin(string username, string password);
    }
}
