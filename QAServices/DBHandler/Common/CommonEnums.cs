﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBHandler.Common
{
    public class CommonEnums
    {
        public enum ProjectStatus
        {
            Submitted = 1,

            OnGoing = 2,

            Complete = 3
        }

        public enum UserRoles
        {
            SuperAdmin = 1,

            Admin = 2,

            User = 3
        }
    }
}