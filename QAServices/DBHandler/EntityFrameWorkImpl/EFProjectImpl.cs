﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBHandler.DBActionsInterfaces;
using DBHandler.DataContexts;

namespace DBHandler.EntityFrameWorkImpl
{
    public class EFProjectImpl : IProject
    {
        private QAServices db = null;

        public EFProjectImpl()
        {
            db = new QAServices();
        }

        public List<Project> GetProjectList()
        {
            return db.Projects.ToList();
        }

        public List<Project> GetProjectList(int id)
        {
            return db.Projects.Where(p=>p.UserId==id).ToList();
        }

        public List<ProjectType> GetProjectTypeList()
        {
            return db.ProjectTypes.ToList();
        }

        public bool SaveProject(Project project)
        {
            try
            {
                db.Projects.Add(project);
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Project FindProject(int id)
        {
            Project project = db.Projects.Find(id);
            return project;
        }

        public bool AddQuotation(int projectId, Quotation quotation)
        {
            Project project = db.Projects.Find(projectId);
            if (project != null)
            {
                project.Quotation = quotation;
                db.SaveChanges();
                return true;
            }

            return false;
        }

        public Quotation GetQuotationDetails(int projectId)
        {
            Project project = db.Projects.Find(projectId);
            if (project != null)
            {
                var quotation = project.Quotation;
                if (quotation != null)
                {
                    return quotation;
                }
            }

            return null;
        }
    }
}
