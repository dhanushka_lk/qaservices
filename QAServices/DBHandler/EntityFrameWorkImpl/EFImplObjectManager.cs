﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.EntityFrameWorkImpl
{
    public class EFImplObjectManager
    {
        public static EFUserImpl CreateEFUserImplObject()
        {
            return new EFUserImpl();
        }

        public static EFProjectImpl CreateEFProjectImplObject()
        {
            return new EFProjectImpl();
        }
    }
}
