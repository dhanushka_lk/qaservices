﻿using DBHandler.Common;
using DBHandler.DataContexts;
using DBHandler.DBActionsInterfaces;
using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using WebMatrix.WebData;

namespace DBHandler.EntityFrameWorkImpl
{
    public class EFUserImpl:IUser
    {
        private QAServices db = null;

        public EFUserImpl()
        {
            db = new QAServices();
        }

        public void CreateUser(User user)
        {

        }

        public User FindUser(int id)
        {
            User user = new User();
            return user;
        }

        public string GetUserRole(string userName)
        {
            var userId = db.Users.Where(u => u.UserName == userName).FirstOrDefault().UserId;

            var result = (from userRoles in db.UsersInRoles
                          join role in db.Roles on userRoles.RoleId equals role.RoleId
                          where userRoles.UserId == userId
                          select role.RoleName).FirstOrDefault();

            return result;
        }

        public bool RegisterUser(string username, string password, string company, string address, string country, string contactno, string email, string userRole,string name)
        {
            try
            {
                WebSecurity.CreateUserAndAccount(username, password, propertyValues: new
                {
                    Name = name,
                    Company = company,
                    Address = address,
                    Country = country,
                    Contactno = contactno,
                    Email = email,
                    Deleted = false,
                    Active = true
                });

                Roles.AddUserToRole(username, CommonEnums.UserRoles.User.ToString());

                WebSecurity.Login(password, password);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Tuple<bool,string> CheckUserLogin(string username, string password)
        {
            try
            {
                if (WebSecurity.Login(username, password, persistCookie: true))
                {
                    var userRole = GetUserRole(username);
                    string url = string.Empty;

                    if (userRole == CommonEnums.UserRoles.Admin.ToString() || userRole == CommonEnums.UserRoles.SuperAdmin.ToString())
                    {
                        url = "AdminProject/AdminDashBoard";
                    }
                    else
                    {
                        url = "UserProject/DashBoard";
                    }

                    return new Tuple<bool, string>(true, url);
                }
                else
                {
                    return new Tuple<bool, string>(false,string.Empty);
                }
            }
            catch (Exception)
            {
                return new Tuple<bool, string>(false, string.Empty);
            }
        }
    }
}
