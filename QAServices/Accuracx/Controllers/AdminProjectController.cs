﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBHandler.Model;
using DBHandler.DataContexts;
using JiraIntegration.JIRAImpl;
using DBHandler.EntityFrameWorkImpl;

namespace Accuracx.Controllers
{
    public class AdminProjectController : Controller
    {
        private QAServices db = new QAServices();

        //
        // GET: /AdminProject/

        public ActionResult AdminDashBoard()
        {
            EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();
            var list = projectImpl.GetProjectList();
            return View(list);
        }

        //
        // GET: /AdminProject/Details/5

        public ActionResult Details(int id = 0)
        {
            EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();
            var project = projectImpl.FindProject(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            string projectStatus = string.Empty;
            ViewBag.ClientMessage = "Pay the installemt";

            if (project.StatusId == (int)DBHandler.Common.CommonEnums.ProjectStatus.Submitted)
            {
                ViewBag.ProjectStatus = DBHandler.Common.CommonEnums.ProjectStatus.Submitted.ToString();
                return View("SubmittedProjectDetails", project);
            }
            else if (project.StatusId == (int)DBHandler.Common.CommonEnums.ProjectStatus.OnGoing)
            {
                projectStatus = DBHandler.Common.CommonEnums.ProjectStatus.OnGoing.ToString();
            }
            else if (project.StatusId == (int)DBHandler.Common.CommonEnums.ProjectStatus.Complete)
            {
                projectStatus = DBHandler.Common.CommonEnums.ProjectStatus.Submitted.ToString();
            }

            ViewBag.ProjectStatus = projectStatus;

            return View(project);
        }

        public ActionResult AddNewQuotation(string estimation, string budget, string notes, string id)
        {
            try
            {
                EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();
                Quotation quotation = new Quotation {Estimation=Convert.ToInt32(estimation),Budget=Convert.ToDecimal(budget),Notes=notes };
                var result = projectImpl.AddQuotation(Convert.ToInt32(id), quotation);
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult GetQuotationDetails(int projectId)
        //{
        //    try
        //    {
        //        EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();
        //        Quotation quotation = new Quotation { Estimation = Convert.ToInt32(estimation), Budget = Convert.ToDecimal(budget), Notes = notes };
        //        var result = projectImpl.AddQuotation(Convert.ToInt32(id), quotation);
        //        return Json(new { result = true }, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception)
        //    {
        //        return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //
        // GET: /AdminProject/Create

        public ActionResult Create()
        {
            ViewBag.ProjectTypeId = new SelectList(db.ProjectTypes, "ProjectTypeId", "ProjectTypeName");
            ViewBag.PaymentPlanId = new SelectList(db.PaymentPlans, "PaymentPlanId", "PaymentPlanName");
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName");
            ViewBag.QuotationId = new SelectList(db.Quotations, "QuotationId", "Notes");
            return View();
        }

        //
        // POST: /AdminProject/Create

        //[HttpPost]
        //public ActionResult Create(Project project)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Projects.Add(project);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.ProjectTypeId = new SelectList(db.ProjectTypes, "ProjectTypeId", "ProjectTypeName", project.ProjectTypeId);
        //    ViewBag.PaymentPlanId = new SelectList(db.PaymentPlans, "PaymentPlanId", "PaymentPlanName", project.PaymentPlanId);
        //    ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", project.UserId);
        //    ViewBag.QuotationId = new SelectList(db.Quotations, "QuotationId", "Notes", project.QuotationId);
        //    return View(project);
        //}

        //
        // GET: /AdminProject/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    Project project = db.Projects.Find(id);
        //    if (project == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ProjectTypeId = new SelectList(db.ProjectTypes, "ProjectTypeId", "ProjectTypeName", project.ProjectTypeId);
        //    ViewBag.PaymentPlanId = new SelectList(db.PaymentPlans, "PaymentPlanId", "PaymentPlanName", project.PaymentPlanId);
        //    ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", project.UserId);
        //    ViewBag.QuotationId = new SelectList(db.Quotations, "QuotationId", "Notes", project.QuotationId);
        //    return View(project);
        //}

        //
        // POST: /AdminProject/Edit/5

        //[HttpPost]
        //public ActionResult Edit(Project project)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(project).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ProjectTypeId = new SelectList(db.ProjectTypes, "ProjectTypeId", "ProjectTypeName", project.ProjectTypeId);
        //    ViewBag.PaymentPlanId = new SelectList(db.PaymentPlans, "PaymentPlanId", "PaymentPlanName", project.PaymentPlanId);
        //    ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", project.UserId);
        //    ViewBag.QuotationId = new SelectList(db.Quotations, "QuotationId", "Notes", project.QuotationId);
        //    return View(project);
        //}

        //
        // GET: /AdminProject/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    Project project = db.Projects.Find(id);
        //    if (project == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(project);
        //}

        //
        // POST: /AdminProject/Delete/5

        [HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Project project = db.Projects.Find(id);
        //    db.Projects.Remove(project);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}