﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBHandler.Model;
using DBHandler.DataContexts;
using DBHandler.EntityFrameWorkImpl;
using DBHandler.Common;
using WebMatrix.WebData;

namespace Accuracx.Controllers
{
    [Authorize(Roles = "User")]
    public class UserProjectController : Controller
    {

        //
        // GET: /UserProject/



        //public ActionResult Index()
        //{
        //    //var projects = db.Projects.Include(p => p.ProjectType).Include(p => p.PaymentPlan).Include(p => p.User).Include(p => p.Quotation);
        //    //return View(projects.ToList());
        //}

        public ActionResult DashBoard()
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();
            var list = projectImpl.GetProjectList(userId);
            return View(list);
        }

        //
        // GET: /UserProject/Details/5

        public ActionResult Details(int id = 0)
        {
            EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();
            var project = projectImpl.FindProject(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            string projectStatus = string.Empty;
            ViewBag.ClientMessage = "Pay the installemt";

            if (project.StatusId == (int)DBHandler.Common.CommonEnums.ProjectStatus.Submitted)
            {
                ViewBag.ProjectStatus = DBHandler.Common.CommonEnums.ProjectStatus.Submitted.ToString();
                return View("SubmittedProjectDetails", project);
            }
            else if (project.StatusId == (int)DBHandler.Common.CommonEnums.ProjectStatus.OnGoing)
            {
                projectStatus = DBHandler.Common.CommonEnums.ProjectStatus.OnGoing.ToString();
            }
            else if (project.StatusId == (int)DBHandler.Common.CommonEnums.ProjectStatus.Complete)
            {
                projectStatus = DBHandler.Common.CommonEnums.ProjectStatus.Submitted.ToString();
            }

            ViewBag.ProjectStatus = projectStatus;

            return View(project);
        }

        //
        // GET: /UserProject/Create

        public ActionResult Create()
        {
            EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();
            var projectTypes = projectImpl.GetProjectTypeList();

            ViewBag.ProjectTypeId = new SelectList(projectTypes, "ProjectTypeId", "ProjectTypeName");
            return View();
        }

        //
        // POST: /UserProject/Create

        [HttpPost]
        public ActionResult Create(Project project)
        {
            EFProjectImpl projectImpl = DBHandler.EntityFrameWorkImpl.EFImplObjectManager.CreateEFProjectImplObject();

            if (ModelState.IsValid)
            {
                project.StatusId = (int)CommonEnums.ProjectStatus.Submitted;
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                project.UserId = userId;
                var result = projectImpl.SaveProject(project);
                return RedirectToAction("DashBoard");
            }

            var projectTypes = projectImpl.GetProjectTypeList();
            ViewBag.ProjectTypeId = new SelectList(projectTypes, "ProjectTypeId", "ProjectTypeName", project.ProjectTypeId);
            return View(project);
        }

        ////
        //// GET: /UserProject/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    Project project = db.Projects.Find(id);
        //    if (project == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ProjectTypeId = new SelectList(db.ProjectTypes, "ProjectTypeId", "ProjectTypeName", project.ProjectTypeId);
        //    ViewBag.PaymentPlanId = new SelectList(db.PaymentPlans, "PaymentPlanId", "PaymentPlanName", project.PaymentPlanId);
        //    ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", project.UserId);
        //    ViewBag.QuotationId = new SelectList(db.Quotations, "QuotationId", "Notes", project.QuotationId);
        //    return View(project);
        //}

        ////
        //// POST: /UserProject/Edit/5

        //[HttpPost]
        //public ActionResult Edit(Project project)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(project).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ProjectTypeId = new SelectList(db.ProjectTypes, "ProjectTypeId", "ProjectTypeName", project.ProjectTypeId);
        //    ViewBag.PaymentPlanId = new SelectList(db.PaymentPlans, "PaymentPlanId", "PaymentPlanName", project.PaymentPlanId);
        //    ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", project.UserId);
        //    ViewBag.QuotationId = new SelectList(db.Quotations, "QuotationId", "Notes", project.QuotationId);
        //    return View(project);
        //}

        ////
        //// GET: /UserProject/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    Project project = db.Projects.Find(id);
        //    if (project == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(project);
        //}

        ////
        //// POST: /UserProject/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Project project = db.Projects.Find(id);
        //    db.Projects.Remove(project);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}