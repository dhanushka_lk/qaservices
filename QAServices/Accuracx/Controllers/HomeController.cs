﻿using DBHandler.DataContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using DBHandler;
using DBHandler.EntityFrameWorkImpl;
using DBHandler.Common;

namespace Accuracx.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string returnUrl)
        {
            if (User.IsInRole(CommonEnums.UserRoles.Admin.ToString()) || User.IsInRole(CommonEnums.UserRoles.SuperAdmin.ToString()))
            {
                return RedirectToAction("AdminDashBoard", "AdminProject");
            }
            else if (User.IsInRole(CommonEnums.UserRoles.User.ToString()))
            {
                return RedirectToAction("DashBoard", "UserProject");
            }

            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            ViewBag.Title = "Accuracx | Home";
            ViewBag.ShowLogin = false;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                ViewBag.ShowLogin = true;
                ViewBag.ReturnUrl = returnUrl;
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CheckUserLogin(string username, string password)
        {
            try
            {
                EFUserImpl eFUserImpl = EFImplObjectManager.CreateEFUserImplObject();

                var result = eFUserImpl.CheckUserLogin(username, password);

                if (result.Item1)
                {
                    return Json(new { result = true, url = result.Item2 }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { result = false, url = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { result = false, url = string.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RegisterUser(string username, string password, string company, string address, string country, string contactno, string email,string name)
        {
            try
            {
                EFUserImpl eFUserImpl = EFImplObjectManager.CreateEFUserImplObject();
                var sucess = eFUserImpl.RegisterUser(username, password, company, address, country, contactno, email, CommonEnums.UserRoles.User.ToString(), name);
                return Json(new { result = sucess }, JsonRequestBehavior.AllowGet);
            }
            catch (MembershipCreateUserException)
            {
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
