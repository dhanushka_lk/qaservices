﻿using DBHandler.DataContexts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using DBHandler.Common;

namespace Accuracx
{
    public class InitSecurityDb : DropCreateDatabaseIfModelChanges<QAServices>
    {
        protected override void Seed(QAServices context)
        {
            WebSecurity.InitializeDatabaseConnection("QAServConn", "User", "UserId", "UserName", autoCreateTables: true);
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists(CommonEnums.UserRoles.SuperAdmin.ToString()))
            {
                roles.CreateRole(CommonEnums.UserRoles.SuperAdmin.ToString());
            }

            if (!roles.RoleExists(CommonEnums.UserRoles.Admin.ToString()))
            {
                roles.CreateRole(CommonEnums.UserRoles.Admin.ToString());
            }

            if (!roles.RoleExists(CommonEnums.UserRoles.User.ToString()))
            {
                roles.CreateRole(CommonEnums.UserRoles.User.ToString());
            }

            IDictionary<string, object> values = new Dictionary<string, object>();

            values.Add("Deleted", "False");
            values.Add("Active", "True");

            if (membership.GetUser("SuperAdmin", false) == null)
            {
                membership.CreateUserAndAccount("SuperAdmin", "123456", values);
            }

            if (!roles.GetRolesForUser("SuperAdmin").Contains("SuperAdmin"))
            {
                roles.AddUsersToRoles(new[] { "SuperAdmin" }, new[] { "SuperAdmin" });
            }

            context.SaveChanges();
        }
    }
}