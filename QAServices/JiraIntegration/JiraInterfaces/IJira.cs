﻿using JIRC.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraIntegration.JiraInterfaces
{
    interface IJira
    {
        IEnumerable<BasicProject> GetProjectList();
    }
}
