﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraIntegration
{
    //public class IssueList
    //{

    //    public IList<Issue> issues { get; set; }
    //}

    //public class Issue
    //{
    //    public string expand { get; set; }
    //    public string id { get; set; }
    //    public string self { get; set; }
    //    public string key { get; set; }
    //    public Fields fields { get; set; }
    //}

    //public class Fields
    //{
    //    public Issuetype issuetype { get; set; }
    //    public string timespent { get; set; }
    //    public Project project { get; set; }
    //    public IList<FixVersions> fixVersions { get; set; }
    //    public string aggregatetimespent { get; set; }
    //    public string resolution { get; set; }
    //    public string resolutiondate { get; set; }
    //    public string workratio { get; set; }
    //    public string lastViewed { get; set; }
    //    public string created { get; set; }
    //    public Priority priority { get; set; }
    //    public string timeestimate { get; set; }
    //    public string aggregatetimeoriginalestimate { get; set; }
    //    public IList<Version> versions { get; set; }
    //    public Assignee assignee { get; set; }
    //    public string updated { get; set; }
    //    public Status status { get; set; }
    //    public IList<Component> components { get; set; }
    //    public string timeoriginalestimate { get; set; }
    //    public string description { get; set; }
    //    public string aggregatetimeestimate { get; set; }
    //    public string summary { get; set; }
    //    public Creator creator { get; set; }
    //    public IList<Subtasks> subtasks { get; set; }
    //    public Reporter reporter { get; set; }
    //    public Aggregateprogress aggregateprogress { get; set; }
    //    public string environment { get; set; }
    //    public string duedate { get; set; }
    //    public Progress progress { get; set; }
    //}

    //public class Issuetype
    //{
    //    public string id { get; set; }
    //    public string name { get; set; }
    //    public bool subtask { get; set; }
    //    public string avatarId { get; set; }
    //    public string description { get; set; }
    //}

    //public class Status
    //{
    //    public string description { get; set; }
    //    public string iconUrl { get; set; }
    //    public string name { get; set; }
    //    public StatusCategory statusCategory { get; set; }
    //}

    //public class Project
    //{
    //    public string id { get; set; }
    //    public string key { get; set; }
    //    public string name { get; set; }
    //    public string description { get; set; }
    //    public Lead lead { get; set; }
    //    public List<Component> components { get; set; }
    //    public List<Issuetype> issuetypes { get; set; }
    //    public string url { get; set; }
    //    public string email { get; set; }
    //    public string assigneeType { get; set; }
    //    public List<Version> versions { get; set; }
    //}

    //public class StatusCategory
    //{
    //    public string key { get; set; }
    //    public string colorName { get; set; }
    //    public string name { get; set; }
    //}

    //public class FixVersions
    //{
    //    public string description { get; set; }
    //    public string name { get; set; }
    //    public string archived { get; set; }
    //    public string released { get; set; }
    //    public string releaseDate { get; set; }
    //}

    //public class Priority
    //{
    //    public string iconUrl { get; set; }
    //    public string name { get; set; }
    //}

    //public class Version
    //{
    //}

    //public class Assignee
    //{
    //    public string name { get; set; }
    //    public string key { get; set; }
    //    public string emailAddress { get; set; }
    //    public string displayName { get; set; }
    //    public string active { get; set; }
    //    public string timeZone { get; set; }
    //}

    //public class Component
    //{
    //    public string id { get; set; }
    //    public string name { get; set; }
    //    public string description { get; set; }
    //    public Lead lead { get; set; }
    //    public string assigneeType { get; set; }
    //    public Assignee assignee { get; set; }
    //    public string realAssigneeType { get; set; }
    //    public Assignee realAssignee { get; set; }
    //    public bool isAssigneeTypeValid { get; set; }

    //}

    //public class Subtasks
    //{
    //}

    //public class Creator
    //{
    //    public string name { get; set; }
    //    public string key { get; set; }
    //    public string emailAddress { get; set; }
    //    public string displayName { get; set; }
    //    public string active { get; set; }
    //    public string timeZone { get; set; }
    //}

    //public class Lead
    //{
    //    public string name { get; set; }
    //    public string displayName { get; set; }
    //    public string active { get; set; }
    //}

    //public class Reporter
    //{
    //    public string name { get; set; }
    //    public string key { get; set; }
    //    public string emailAddress { get; set; }
    //    public string displayName { get; set; }
    //    public string active { get; set; }
    //    public string timeZone { get; set; }
    //}

    //public class Aggregateprogress
    //{
    //    public string progress { get; set; }
    //    public string total { get; set; }
    //    public string percent { get; set; }
    //}

    //public class Progress
    //{
    //    public string progress { get; set; }
    //    public string total { get; set; }
    //    public string percent { get; set; }
    //}
}
