﻿using JiraIntegration.JiraInterfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Newtonsoft.Json;
using JIRC;
using JIRC.Domain;

namespace JiraIntegration.JIRAImpl
{
    public class JiraImpl : IJira
    {
        IJiraRestClient client = null;

        public JiraImpl()
        {
            client = JiraRestClientFactory.CreateWithBasicHttpAuth(new Uri("https://softvil.atlassian.net"), "ishara", "IsharaSampath7?");
        }

        public IEnumerable<BasicProject> GetProjectList()
        {
            var projectList = client.ProjectClient.GetAllProjects();
            return projectList;
        }

        //static string argument = null;
        //static string data = null;
        //static string method = "GET";

        //public IEnumerable<BasicProject> GetProjectList()
        //{
        //    string url = WebConfigurationManager.AppSettings["url"];

        //    //url = url + "search?jql=reporter=ishara";

        //    //HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
        //    //request.ContentType = "application/json";
        //    //request.Method = method; 

        //    //if (data != null)
        //    //{
        //    //    using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
        //    //    {
        //    //        writer.Write(data);
        //    //    }
        //    //}

        //    //string base64Credentials = GetEncodedCredentials();
        //    //request.Headers.Add("Authorization", "Basic " + base64Credentials);

        //    //HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        //    //string result = string.Empty;
        //    //using (StreamReader reader = new StreamReader(response.GetResponseStream()))
        //    //{
        //    //    result = reader.ReadToEnd();
        //    //}

        //    //IssueList issueList = JsonConvert.DeserializeObject<IssueList>(result);

        //    //return issueList;

        //    //Jira jira = new Jira("https://softvil.atlassian.net", "ishara", "IsharaSampath7?");

        //    var client = JiraRestClientFactory.CreateWithBasicHttpAuth(new Uri("https://softvil.atlassian.net"), "ishara", "IsharaSampath7?");

        //    var tstPrj = client.ProjectClient.GetAllProjects();

        //    return tstPrj;
        //}

        //public List<Project> GetProjectList()
        //{
        //    string url = WebConfigurationManager.AppSettings["url"];

        //    url = url + "project";

        //    HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
        //    request.ContentType = "application/json";
        //    request.Method = method;

        //    if (data != null)
        //    {
        //        using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(data);
        //        }
        //    }

        //    string base64Credentials = GetEncodedCredentials();
        //    request.Headers.Add("Authorization", "Basic " + base64Credentials);

        //    HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        //    string result = string.Empty;
        //    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
        //    {
        //        result = reader.ReadToEnd();
        //    }

        //    List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(result);

        //    return projects;
        //}

        //public Project GetProjectDetails(string id)
        //{
        //    string url = WebConfigurationManager.AppSettings["url"];

        //    url = url + "project/"+id;

        //    HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
        //    request.ContentType = "application/json";
        //    request.Method = method;

        //    if (data != null)
        //    {
        //        using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(data);
        //        }
        //    }

        //    string base64Credentials = GetEncodedCredentials();
        //    request.Headers.Add("Authorization", "Basic " + base64Credentials);

        //    HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        //    string result = string.Empty;
        //    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
        //    {
        //        result = reader.ReadToEnd();
        //    }

        //    Project project = JsonConvert.DeserializeObject<Project>(result);

        //    return project;
        //}

        ////public IssueList GetProjectProgress(string id)
        ////{
        ////    string url = WebConfigurationManager.AppSettings["url"];

        ////    url = url + "search?jql=project="+id;

        ////    HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
        ////    request.ContentType = "application/json";
        ////    request.Method = method;

        ////    if (data != null)
        ////    {
        ////        using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
        ////        {
        ////            writer.Write(data);
        ////        }
        ////    }

        ////    string base64Credentials = GetEncodedCredentials();
        ////    request.Headers.Add("Authorization", "Basic " + base64Credentials);

        ////    HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        ////    string result = string.Empty;
        ////    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
        ////    {
        ////        result = reader.ReadToEnd();
        ////    }

        ////    IssueList project = JsonConvert.DeserializeObject<IssueList>(result);

        ////    return project;
        ////}

        //private static string GetEncodedCredentials()
        //{
        //    string mergedCredentials = string.Format("{0}:{1}", "ishara", "IsharaSampath7?");
        //    byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(mergedCredentials);
        //    return Convert.ToBase64String(byteCredentials);
        //}
    }
}
